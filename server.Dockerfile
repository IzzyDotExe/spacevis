FROM node:18-alpine3.19 as build-step

WORKDIR /app

COPY ./client/package*.json .

RUN npm ci

COPY ./client . 

RUN npm run build

FROM node:18-alpine3.19 
WORKDIR /app

COPY ./server/package*.json .
RUN npm ci

COPY ./server .

COPY --from=build-step /app/build /client/build

ENTRYPOINT [ "node", "bin/www" ]
