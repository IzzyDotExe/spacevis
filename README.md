# Beyond our world

Express + React app

Description: Inspect and manipulate data about meteorites. You will have access to a 3d globe representation of over 45k meteorite impact zones over the span of over 1100 years.

861 AD - 2023 AD

## Running in docker!

To run in docker first you must set up a volume to store all the data in
create it like so:

```bash
docker volume create volume_name
```

Then you need to create your `.env` file. Its important to know and keep your `volume_name` so that you can set it in the environment. Place the `.env` in the `/server` directory. 

.env example:

```
ATLAS_URI=mongodb://root:<chosen_password>@mongo_temp:27017/space?authSource=admin
MONGO_INITDB_ROOT_PASSWORD=<chosen_password>
DOCKER_VOLUME=<volume_name>
SEED_DATA_FILE_PATH='./src/utils/data/meteorites.csv'

PORT=80
BIND_PORT=80
```

next you can build and run the compose seeder.

```bash
cd server
docker compose -f seed.compose.yml build
docker compose -f seed.compose.yml up
```

Once you see that all the meteorites were inserted, quit the seeder using ^c

next you can build and run the full app using
```bash
cd server
docker compose -f docker-compose.yml build
docker compose -f docker-compose.yml up
```
And now you are running the full app along with a database exploerer on port 3001